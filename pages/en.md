- @title = 'Home'
- @this.layout = 'home'
- @this.toc = false

##### Riseup Labs is a 501(c)3 non-profit focused on technological research, development, and education for the purpose of furthering social justice and supporting social movements. 

### Support Us! - [Make a donation in support of our work](donate)
<p>
### Get involved! - [Learn more about projects we work with](projects)
<p>
### Say hi! - [Contact Us!]("mailto:labs@riseup.net") - labs@riseup.net

###### Riseup Labs is the sister organization to Riseup Networks, the birds behind [riseup.net](https://riseup.net) 
