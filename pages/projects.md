- @title = 'Donate'
- @this.layout = 'home'
- @this.toc = false

<p align="right"><b>Projects associated with Riseup Labs</b>
</p>

####  LEAP VPN
> A white label, personal VPN service for censorship circumvention, location anonymization and traffic encryption, available for Android, Windows, Mac and Linux. Liberate, in partnership with [LEAP Encryption Access Project](https://leap.se/), contributes to the development of LEAP VPN. Branded versions of LEAP VPN are provided by trusted service providers [Riseup](https://riseup.net), the [Calyx Institute](https://calyxinstitute.org/) and [LibraryVPN](https://libraryvpn.org/).

#### TREES 
>(Technology for Resting Email Encrypted Storage) adds individually encrypted mail storage to the Dovecot IMAP server. This makes it so that email is only readable by the server when the user has an active IMAP session. [Go to project site](https://0xacab.org/riseuplabs/trees) 

#### Nest
>A user management system that supports email, recovery codes, invite codes, U2F authentication, single sign on, and user data that is personally encrypted. It designed to be the central integration point for independent service providers. Source code is forthcoming, once a security audit is finished.

#### Crabgrass 
>A software libre web application designed for social networking, group collaboration and network organizing. Our goal is to create communication tools that are tailored specifically to meet the needs of bottom up grassroots organizing. [Go to project site](https://0xacab.org/riseuplabs/crabgrass)

#### Monkeysphere 
>Enables you to use the OpenPGP web of trust to verify ssh connections. SSH key-based auth entication is tried-and-true, but it lacks a true public key infrastructure for key certification, revocation, and expiration. Monkeysphere is a framework that uses the OpenPGP web of trust for these PKI functions. [Go to project site](http://web.monkeysphere.info/)

#### Backupninja 
>Coordinate system backup by dropping a few simple files into /etc/backup.d/. Backupninja provides a centralized way to configure and schedule many different backup utilities. [Go to project site](https://0xacab.org/riseuplabs/backupninja)

#### Tails
>A live operating system that you can start on almost any computer from a USB stick or a DVD. It aims at preserving your privacy and anonymity. Riseup Labs is a fiscal sponsor for some of the Tails development. [Go to project site](https://tails.boum.org/)
