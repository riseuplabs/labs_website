- @title = 'Donate'
- @this.layout = 'home'
- @this.toc = false



## Donations by Mail

Send checks or money orders payable to:  

>Riseup Labs  
>PO Box 4282  
>Seattle, WA 98194 USA


## Online donations

### Via NetworkForGood

<p><a href="https://www.networkforgood.org/donate/process/expressDonation.aspx?ORGID2=20-4204809">Click here to donate</a></p>

### Via Paypal

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="T9JN8NW7FW9CQ" />
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
</form>

##### Please know that we are so incredibly grateful: your support is our lifeblood and makes all our hard work possible. We love you for putting your money where your values are! We aren't able to send thank you notes, but know that our appreciation is in our work.

##### Let us know if you would like to make a donation in support of a particular project we are support. [See current projects](http://riseuplabs.org/projects) 

##### Some fine print: _We are required by law to annually report the identity of any donor who gives more than $5,000 USD. If you need documenation of your donation for tax purposes, please let us know._ 
